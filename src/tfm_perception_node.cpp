#include "perception_controller.h"

int main(int argc, char **argv)
{

  ros::init(argc, argv, "perception");
 
  PerceptionController perception(argc, argv);

  // Rate to keep publishing the differential wheeled robot (wheelchair) tf
  // The callbacks in the path controller are responsible for updating the control input
  ros::Rate loop_rate(50);
  while (ros::ok())
  {
    perception.publishControlInput();

    ros::spinOnce();

    loop_rate.sleep();
  }

  return 0;
}
