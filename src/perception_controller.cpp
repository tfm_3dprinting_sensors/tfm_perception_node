#include "perception_controller.h"

PerceptionController::PerceptionController(int argc, char**argv)
{
    // init atributes

    // Sensor 1 subscriber
    sensor1_sub_ = n_.subscribe("/arduino/sensor1/voltage", 1000, &PerceptionController::sensor1Callback, this);
    // Sensor 2 subscriber
    sensor2_sub_ = n_.subscribe("/arduino/sensor2/voltage", 1000, &PerceptionController::sensor2Callback, this);
    
    // Tf subscriber
    tf_sub_ = n_.subscribe("/velocity_controller/odom", 1000, &PerceptionController::tfCallback, this);
    
    // Differential wheeled robot control input publisher
    control_pub_ = n_.advertise<geometry_msgs::Twist>("/velocity_controller/cmd_vel", 1000);
    
    // Visualization publisher
    vis_pub_ = n_.advertise<visualization_msgs::MarkerArray>("visualization_marker", 1000);

    // init
    twist_.linear.x = 0.0;
    twist_.linear.y = 0.0;
    twist_.linear.z = 0.0;
    twist_.angular.x = 0.0;
    twist_.angular.y = 0.0;
    twist_.angular.z = 0.0;
    
    count_ = 0;

    ROS_INFO("Perception controller started");

};

void PerceptionController::sensor1Callback(const std_msgs::Float64::ConstPtr& msg)
{
    //update twist linear x
    if (msg->data > 0.0) {
      twist_.linear.x = msg->data;
    } else {
      twist_.linear.x = 0.0;
    }
};

void PerceptionController::sensor2Callback(const std_msgs::Float64::ConstPtr& msg)
{
    //update twist angular z
    if (msg->data > 1.45) {
      twist_.angular.z = msg->data;
    } else {
      twist_.angular.z = 0.0;
    }
};

void PerceptionController::tfCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
  if (count_ == 0 || count_ % 1000 == 0) {
    visualization_msgs::Marker marker;
    marker.header.frame_id = "odom";
    marker.header.stamp = ros::Time();
    marker.id = count_;
    marker.type = visualization_msgs::Marker::SPHERE;
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose = msg->pose.pose;
    marker.scale.x = 0.1;
    marker.scale.y = 0.1;
    marker.scale.z = 0.1;
    marker.color.a = 1.0;
    marker.color.r = 1.0;
    marker.color.g = 0.0;
    marker.color.b = 0.0;
    marker.lifetime = ros::Duration();

    marker_a_.markers.push_back(marker);
    vis_pub_.publish(marker_a_);
    count_++;
  }

};

void PerceptionController::publishControlInput()
{
    control_pub_.publish(twist_);
};
