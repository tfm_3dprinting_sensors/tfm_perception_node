#ifndef PERCEPTION_CONTROLLER_H_
#define PERCEPTION_CONTROLLER_H_

#include "ros/ros.h"
#include "std_msgs/Float64.h"
#include "geometry_msgs/Twist.h"
#include "geometry_msgs/Transform.h"
#include "nav_msgs/Odometry.h"
#include <tf/LinearMath/Vector3.h>
#include <tf/tfMessage.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <cmath>

class PerceptionController
{
    private:
      
      // node handle
      ros::NodeHandle n_;

      // Subscribers
      ros::Subscriber sensor1_sub_;
      ros::Subscriber sensor2_sub_;
      ros::Subscriber tf_sub_;

      // Publishers
      ros::Publisher control_pub_;
      ros::Publisher vis_pub_;
    
      // atributes
      geometry_msgs::Twist twist_;
      visualization_msgs::MarkerArray marker_a_;
      int count_;

      // methods
   
    public:

      // callbacks
      void sensor1Callback(const std_msgs::Float64::ConstPtr& msg);
      void sensor2Callback(const std_msgs::Float64::ConstPtr& msg);
      void tfCallback(const nav_msgs::Odometry::ConstPtr& msg);
      
      // public methods
      void publishControlInput();

      // constructor
      PerceptionController(int argc, char**argv);
   
};

#endif
