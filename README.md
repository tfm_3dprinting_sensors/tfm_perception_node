# tfm_perception_node

:exclamation: Please, to use this repository start with doing the setup defined in https://gitlab.com/tfm_3dprinting_sensors/tfm_workspace_setup.

## Repository structure

This repository contains a Perception ROS node.

## Authors and acknowledgment
Oriol Vendrell

## License
BSD 2-Clause "Simplified" License

## Project status
This project is part of my final master's thesis.
